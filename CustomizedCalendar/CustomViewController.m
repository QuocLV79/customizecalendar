//
//  CustomViewController.m
//  Example
//
//  Created by Jonathan Tribouharet.
//
#define mainGreenColor [UIColor colorWithRed:116.0f/256.0f green:188.0f/256.0f blue:62.0f/256.0f alpha:1.0f]
#define grayColorForDotCalendar [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1]
#import "CustomViewController.h"

@interface CustomViewController (){
    NSMutableDictionary *_eventsByDate;
    
    NSDate *dateSelected,*todayDate;
}

@end

@implementation CustomViewController
#pragma mark - Life Cycle
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(!self){
        return nil;
    }
    
    self.title = @"Custom";
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    [self setupData];

}
- (void)configUI {

}
- (void)setupData {
    todayDate = [NSDate date];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatSingle;
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:todayDate];
}
#pragma mark - CalendarManager delegate
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.dotView.backgroundColor = grayColorForDotCalendar;
    // Other month
        dayView.hidden = [dayView isFromAnotherMonth];
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        if ([[[NSCalendar currentCalendar] components: NSCalendarUnitWeekday fromDate: dayView.date] weekday]==1||[[[NSCalendar currentCalendar] components: NSCalendarUnitWeekday fromDate: dayView.date] weekday]==7) {
        }
    }
    // Selected date
    else if(dateSelected && [_calendarManager.dateHelper date:dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    } else if ([_calendarManager.dateHelper dateIsWeekend:dayView.date]) {
        dayView.textLabel.textColor = [UIColor redColor];
    } else {
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
//    dateSelected = dayView.date;
////    NSLog(@"%tu",[[[NSCalendar currentCalendar] components: NSCalendarUnitWeekday fromDate: dayView.date] weekday]);
//    // Animation for the circleView
//    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
//    [UIView transitionWithView:dayView
//                      duration:.3
//                       options:0
//                    animations:^{
//                        dayView.circleView.transform = CGAffineTransformIdentity;
//                        [_calendarManager reload];
//                    } completion:nil];
//    
//    
//    // Load the previous or next page if touch a day from another month
//    
//    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
//        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
//            [_calendarContentView loadNextPageWithAnimation];
//        }
//        else{
//            [_calendarContentView loadPreviousPageWithAnimation];
//        }
//    }
}
#pragma mark - Buttons
- (IBAction)today:(id)sender {
    [_calendarManager setDate:todayDate];
}
#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    menuItemView.contentMode = UIViewContentModeRight;
    menuItemView.text = [dateFormatter stringFromDate:date];
    menuItemView.textColor = mainGreenColor;
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *weekDayView = [JTCalendarWeekDayView new];
    for (int i = 0; i<weekDayView.dayViews.count; i++) {
        UILabel *label = (UILabel*)weekDayView.dayViews[i];
        label.font = [UIFont boldSystemFontOfSize:10];
        (i==0||i==weekDayView.dayViews.count-1)?(label.textColor = [UIColor redColor]):(label.textColor = [UIColor blackColor]);
    }
    return weekDayView;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    view.textLabel.font = [UIFont systemFontOfSize:16];
    view.circleRatio = .6;
    return view;
}
- (UIView<JTCalendarWeek> *)calendarBuildWeekView:(JTCalendarManager *)calendar {
    JTCalendarWeekView *view = [JTCalendarWeekView new];
    return view;
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}

@end
