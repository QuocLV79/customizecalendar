//
//  HomeVC.m
//  CustomizedCalendar
//
//  Created by Quoc LV on 11/26/15.
//  Copyright © 2015 Quoc LV. All rights reserved.
//

#import "HomeVC.h"
#import "JTCalendar.h"
@interface HomeVC (){


    NSMutableDictionary *eventsByDate;
    NSDate *todayDate;
    NSDate *_dateSelected;
}
@end

@implementation HomeVC
#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setupData];
}
- (void)configUI {
//    [self createRandomEvents];
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;

    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
}
- (void)setupData {

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - JT Calendar
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView*)dayView {
//    dayView.hidden = [dayView isFromAnotherMonth];
    // Today
    
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
//        NSLog(@"%@\n%@",[NSDate date],dayView.date);
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
//    if([self haveEventForDay:dayView.date]){
//        dayView.dotView.hidden = NO;
//    }
//    else{
//        dayView.dotView.hidden = YES;
//    }
}
- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *weekDayView = [JTCalendarWeekDayView new];
    for (int i = 0; i<weekDayView.dayViews.count; i++) {
        UILabel *label = (UILabel*)weekDayView.dayViews[i];
        label.font = [UIFont boldSystemFontOfSize:10];
        (i==0||i==weekDayView.dayViews.count-1)?(label.textColor = [UIColor redColor]):(label.textColor = [UIColor blackColor]);
    }
    return weekDayView;
}
//- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
//{
//    JTCalendarDayView *view = [JTCalendarDayView new];
//    return view;
//}
//- (UIView*)calendarBuildMenuItemView:(JTCalendarManager *)calendar {
//    UILabel *menuLabel = [UILabel new];
//    menuLabel.textAlignment = NSTextAlignmentCenter;
//    menuLabel.font = [UIFont boldSystemFontOfSize:16];
//    menuLabel.textColor = [UIColor greenColor];
//    
//    return menuLabel;
//}
- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";

        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }

    menuItemView.text = [dateFormatter stringFromDate:date];
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
//- (NSDateFormatter *)dateFormatter
//{
//    static NSDateFormatter *dateFormatter;
//    if(!dateFormatter){
//        dateFormatter = [NSDateFormatter new];
//        dateFormatter.dateFormat = @"dd-MM-yyyy";
//    }
//    
//    return dateFormatter;
//}
//
//- (BOOL)haveEventForDay:(NSDate *)date
//{
//    NSString *key = [[self dateFormatter] stringFromDate:date];
//    
//    if(eventsByDate[key] && [eventsByDate[key] count] > 0){
//        return YES;
//    }
//    
//    return NO;
//    
//}
//
//- (void)createRandomEvents
//{
//    eventsByDate = [NSMutableDictionary new];
//    
//    for(int i = 0; i < 30; ++i){
//        // Generate 30 random dates between now and 60 days later
//        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
//        
//        // Use the date as key for eventsByDate
//        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
//        
//        if(!eventsByDate[key]){
//            eventsByDate[key] = [NSMutableArray new];
//        }
//        
//        [eventsByDate[key] addObject:randomDate];
//    }
//}
- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;

    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}
- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    view.frame = CGRectMake(0, 0, 70, 70);
    view.circleRatio = .8;
    view.dotRatio = 1. / .9;
    return view;
}
@end
