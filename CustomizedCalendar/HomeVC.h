//
//  HomeVC.h
//  CustomizedCalendar
//
//  Created by Quoc LV on 11/26/15.
//  Copyright © 2015 Quoc LV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"

@interface HomeVC : UIViewController<JTCalendarDelegate>
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (strong,nonatomic)JTCalendarManager *calendarManager;

@end
